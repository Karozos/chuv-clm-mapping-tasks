# Mapping Task for CHUV-CLM

This is the first version of the mapping tasks for the CHUV-CLM hospital. 

## Input files

The files that needed in 

- `DB_EHR-subset_cp.csv`
- `DB_demo.csv`
- `cdes_clm.csv`
- `clm_metadata.csv`

The files `DB_EHR-subset_cp.csv`, `DB_demo.csv` contain the actual hospital's EHR data and are provided by the hospital. The files `cdes_clm.csv`, `clm_metadata.csv` contain metadata about the hospital's and CDE variables and are located in `metadata_files` folder of the current repo. All these files must be placed in the same EHR DataFactory input folder (ie `/data/DataFactory/EHR/input/1` for the first batch of ehr data)

`DB_EHR-subset_cp.csv` headers are:

"VISIT_ID","SUBJECT_CODE","SUBJ_AGE_YEARS_ROUND","VISIT_DATE","SEX","DIAG_etiology_1","DIAG_etiology_2","DIAG_etiology_3","DIAG_lng","DIAG_lngapp","DIAG_stade","DIAG_stademin","DIAG_syndamn","DIAG_syndoth","DIAG_syndr","MoCA_value","MMSE_value"

`DB_demo.csv` headers are:

"SUBJECT_CODE", "GENDER"

## Mapping tasks configuration files

### Preprocess step configuration files

In hospital server we create a new folder in `opt/DataFactory/mipmap_mappings/preprocess_step` and named it accordingly (ie `1` if is the first version of this configuration) and then place in the configuration files that are inside `preprocess_step` folder of this repo.

### Capture step configuration files

In hospital server we create a new folder in `opt/DataFactory/mipmap_mappings/capture_step` and named it accordingly (ie `1` if is the first version of this configuration) and then place in the configuration files that are inside `capture_step` folder of this repo.

### Harmonize step configuration files

In hospital server we create a new folder in `opt/DataFactory/mipmap_mappings/harmonize_step` and named it accordingly (ie `1` if is the first version of this configuration) and then place in the configuration files that are inside `harmonize_step` folder of this repo.
